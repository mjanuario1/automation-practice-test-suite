from pages.sign_in import SignInPage
from pages.create_account import CreateAccountPage
from pages.header import Header
import uuid

def test_empty_email_input_create_account(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to create an account without informing email
  sign_in_page.create_account()

  #Then a message is displayed stating that the email is invalid
  error_message = sign_in_page.get_error_message_create_account()
  assert error_message.text == 'Invalid email address.'

def test_existing_account(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user informs an email of an existing account
  sign_in_page.fill_email_create_account("marcelajanuario92@hotmail.com")
  sign_in_page.create_account()

  #Then a message appears stating that the account already exists
  error_message = sign_in_page.get_error_message_create_account()
  assert error_message.text == 'An account using this email address has already been registered. Please enter a valid password or request a new one.'

  
def test_create_account_successfully(browser):
  sign_in_page = SignInPage(browser)
  create_account_page = CreateAccountPage(browser)
  header = Header(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user informs an email not registered yet
  email_create_account = str(uuid.uuid4()) + '@mail.com'
  sign_in_page.fill_email_create_account(email_create_account)
  sign_in_page.create_account()
  
  #And fill in the necessary personal information
  create_account_page.select_title()
  create_account_page.fill_first_name("Nome")
  create_account_page.fill_last_name("Sobrenome")
  create_account_page.fill_password("aaaaa")
  create_account_page.fill_date_of_birth()
  create_account_page.fill_address("Rua")
  create_account_page.fill_city("Cidade")
  create_account_page.fill_state()
  create_account_page.fill_postal_code("00000")
  create_account_page.fill_mobile_phone("999")
  create_account_page.register()

  #Then the account is created and the login is done automatically
  full_name = header.get_full_name()
  assert full_name.text == 'Nome Sobrenome'
  assert "My account" in browser.title
  header.sign_out()
  
