from pages.sign_in import SignInPage
from pages.forgot_password import ForgotPasswordPage
from pages.header import Header
import uuid

def test_forgot_password(browser):
  sign_in_page = SignInPage(browser)
  forgot_password_page = ForgotPasswordPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user does not remember the password
  sign_in_page.forgot_password()
  forgot_password_page.fill_email_input("marcelajanuario92@hotmail.com")
  forgot_password_page.retrieve_password()

  #Then it is possible to recover the password
  confirmation_message = forgot_password_page.get_confirmation_message()
  assert confirmation_message.text == 'A confirmation email has been sent to your address: marcelajanuario92@hotmail.com'
  

def test_empty_email_input_login(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to login without informing email
  sign_in_page.sign_in()

  #Then a message is displayed stating that an email is required
  error_message_email_required = sign_in_page.get_error_message_login()
  assert error_message_email_required.text == 'An email address required.'

def test_empty_password_input_login(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to login without informing password
  sign_in_page.fill_email_login("marcelajanuario92@hotmail.com")
  sign_in_page.sign_in()

  #Then a message is displayed stating that password is required
  error_message_password_required = sign_in_page.get_error_message_login()
  assert error_message_password_required.text == 'Password is required.'

def test_invalid_email_input_login(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to login with an invalid email
  sign_in_page.fill_email_login("aa")
  sign_in_page.sign_in()

  #Then a message is displayed stating that the email is invalid
  error_message_email_invalid = sign_in_page.get_error_message_login()
  assert error_message_email_invalid.text == 'Invalid email address.'

def test_incorrect_password(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to login with incorrect password
  sign_in_page.fill_email_login("marcelajanuario92@hotmail.com")
  sign_in_page.fill_password("bbbbb")
  sign_in_page.sign_in()

  #Then a message is displayed stating that the authentication failed
  error_message_incorret_password = sign_in_page.get_error_message_login()
  assert error_message_incorret_password.text == 'Authentication failed.'

def test_nonexistent_email(browser):
  sign_in_page = SignInPage(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user tries to login with a nonexistent email
  email_login = str(uuid.uuid4()) + '@mail.com'
  sign_in_page.fill_email_login(email_login)
  sign_in_page.fill_password("bbbbb")
  sign_in_page.sign_in()

  #Then a message is displayed stating that the authentication failed
  error_message_incorret_password = sign_in_page.get_error_message_login()
  assert error_message_incorret_password.text == 'Authentication failed.'

def test_login_successfully(browser):
  sign_in_page = SignInPage(browser)
  header = Header(browser)

  #Given the sign in page is displayed
  sign_in_page.load()

  #When the user informs correct email and password
  sign_in_page.fill_email_login("marcelajanuario92@hotmail.com")
  sign_in_page.fill_password("aaaaa")
  sign_in_page.sign_in()

  #Then the login happens successfully
  full_name = header.get_full_name()
  assert full_name.text == 'Marcela Januário'
  assert "My account" in browser.title
  header.sign_out()




