import pytest
import selenium.webdriver


@pytest.fixture
def browser():

  # Initialize the ChromeDriver instance
  instance = selenium.webdriver.Chrome()

  # Return the WebDriver instance for the setup
  yield instance

  # Quit the WebDriver instance for the cleanup
  instance.quit()