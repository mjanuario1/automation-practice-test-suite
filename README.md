# automation-practice-test-suite 
Este projeto inclui a suite de testes para criação de conta e login em [Automation practice](http://automationpractice.com/index.php?controller=authentication&amp).


## Instruções de setup

### Python

Para utilização deste projeto é necessária a versão 3.9 do Python ou superior. Esta versão pode ser baixada em [Python.org](https://www.python.org/downloads/).

Este projeto também necessita do [pipenv](https://docs.pipenv.org/) para instalação das dependências. 

### WebDriver

Devido à falta de conhecimento sobre os usuários do Automation Practice, este projeto foi construiído apenas para o navegador Chrome. Para utilização deste projeto é necessário ter instalado uma versão do [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/) compatível com a versão do Chrome da máquina na qual o projeto será executado. 

## Executando o projeto

Dentro da raiz do projeto execute:

- `pipenv install` para instalação das dependências
- `pipenv run python -m pytest` para execução dos testes 




