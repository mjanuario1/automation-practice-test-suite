from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select

class CreateAccountPage:

  # Locators

  TITLE = (By.ID, 'id_gender1')
  FIRST_NAME_INPUT = (By.ID, 'customer_firstname')
  LAST_NAME_INPUT = (By.ID, 'customer_lastname')
  PASSWORD_INPUT = (By.ID, 'passwd')
  DATE_OF_BIRTH_DAY = (By.ID, 'days')
  DATE_OF_BIRTH_MONTH = (By.ID, 'months')
  DATE_OF_BIRTH_YEAR = (By.ID, 'years')
  ADDRESS_INPUT = (By.ID, 'address1')
  CITY_INPUT = (By.ID, 'city')
  STATE = (By.ID, 'id_state')
  POSTAL_CODE_INPUT = (By.ID, 'postcode')
  MOBILE_PHONE_INPUT = (By.ID, 'phone_mobile')
  REGISTER_BUTTON = (By.ID, 'submitAccount')


  # Initializer

  def __init__(self, browser):
    self.browser = browser
    self.wait = WebDriverWait(self.browser, 10)

  # Interaction Methods
  
  def select_title(self):
    title = self.wait.until(EC.visibility_of_element_located(self.TITLE))
    title.click()
  
  def fill_first_name(self, first_name):
    fist_name_input = self.wait.until(EC.visibility_of_element_located(self.FIRST_NAME_INPUT))
    fist_name_input.send_keys(first_name)
  
  def fill_last_name(self, last_name):
    last_name_input = self.wait.until(EC.visibility_of_element_located(self.LAST_NAME_INPUT))
    last_name_input.send_keys(last_name)
  
  def fill_password(self, password):
    password_input = self.wait.until(EC.visibility_of_element_located(self.PASSWORD_INPUT))
    password_input.send_keys(password)

  def fill_date_of_birth(self):
    day = Select(self.browser.find_element(*self.DATE_OF_BIRTH_DAY))
    day.select_by_value('1')
    month = Select(self.browser.find_element(*self.DATE_OF_BIRTH_MONTH))
    month.select_by_value('1')
    year = Select(self.browser.find_element(*self.DATE_OF_BIRTH_YEAR))
    year.select_by_value('1998')

  def fill_address(self, address):
    address_input = self.wait.until(EC.visibility_of_element_located(self.ADDRESS_INPUT))
    address_input.send_keys(address)
  
  def fill_city(self, city):
    city_input = self.wait.until(EC.visibility_of_element_located(self.CITY_INPUT))
    city_input.send_keys(city)
  
  def fill_state(self):
    state = Select(self.browser.find_element(*self.STATE))
    state.select_by_value('1')

  def fill_postal_code(self, postal_code):
    postal_code_input = self.wait.until(EC.visibility_of_element_located(self.POSTAL_CODE_INPUT))
    postal_code_input.send_keys(postal_code)
  
  def fill_mobile_phone(self, mobile_phone):
    mobile_phone_input = self.wait.until(EC.visibility_of_element_located(self.MOBILE_PHONE_INPUT))
    mobile_phone_input.send_keys(mobile_phone)
  
  def register(self):
    register_button = self.wait.until(EC.visibility_of_element_located(self.REGISTER_BUTTON))
    register_button.click()
    

  

  
  







  
