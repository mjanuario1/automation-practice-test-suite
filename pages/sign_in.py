from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

class SignInPage:

  # URL

  URL = 'http://automationpractice.com/index.php?controller=authentication&amp'

  # Locators

  CREATE_ACCOUNT_BUTTON = (By.ID, 'SubmitCreate')
  CREATE_ACCOUNT_ERROR_MESSAGE = (By.ID, 'create_account_error')
  CREATE_ACCOUNT_EMAIL_INPUT = (By.ID, 'email_create')
  LOGIN_EMAIL_INPUT = (By.ID, 'email')
  PASSWORD_INPUT = (By.ID, 'passwd')
  SIGN_IN_BUTTON = (By.ID, 'SubmitLogin')
  EMAIL_REQUIRED_ERROR_MESSAGE = (By.XPATH, '//*[@id="center_column"]/div[1]/ol/li')
  FORGOT_PASSWORD = (By.CSS_SELECTOR, 'p.lost_password.form-group')

  # Initializer

  def __init__(self, browser):
    self.browser = browser

  # Interaction Methods

  def load(self):
    self.browser.get(self.URL)

  def create_account(self):
    create_account_button = self.browser.find_element(*self.CREATE_ACCOUNT_BUTTON)
    create_account_button.click()
  
  def get_error_message_create_account(self):
    wait = WebDriverWait(self.browser, 10)
    create_account_error_message = wait.until(EC.visibility_of_element_located(self.CREATE_ACCOUNT_ERROR_MESSAGE))
    create_account_error_message_list = create_account_error_message.find_element(By.TAG_NAME, 'li')
    return create_account_error_message_list

  def fill_email_create_account(self, email):
    create_account_email_input = self.browser.find_element(*self.CREATE_ACCOUNT_EMAIL_INPUT)
    create_account_email_input.send_keys(email)
  
  def fill_email_login(self, email):
    login_email_input = self.browser.find_element(*self.LOGIN_EMAIL_INPUT)
    login_email_input.send_keys(email)
  
  def fill_password(self, password):
    password_input = self.browser.find_element(*self.PASSWORD_INPUT)
    password_input.send_keys(password)
  
  def sign_in(self):
    sign_in_button = self.browser.find_element(*self.SIGN_IN_BUTTON)
    sign_in_button.click()
  
  def forgot_password(self):
    wait = WebDriverWait(self.browser, 10)
    forgot_password = wait.until(EC.visibility_of_element_located(self.FORGOT_PASSWORD))
    forgot_password_link = forgot_password.find_element(By.TAG_NAME, 'a')
    forgot_password_link.click()
  
  def get_error_message_login(self):
    error_message_email_required = self.browser.find_element(*self.EMAIL_REQUIRED_ERROR_MESSAGE)
    return error_message_email_required
  


    
    

 



  