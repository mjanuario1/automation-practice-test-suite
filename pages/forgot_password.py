from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class ForgotPasswordPage:

   # Locators

  EMAIL = (By.ID, 'email')
  RETRIEVE_PASSWORD_BUTTON = (By.CSS_SELECTOR, '#form_forgotpassword > fieldset > p > button')
  CONFIRMATION_MESSAGE = (By.CSS_SELECTOR, '#center_column > div > p')

  # Initializer

  def __init__(self, browser):
    self.browser = browser

  # Interaction Methods
  
  def fill_email_input(self, email):
    email_input = self.browser.find_element(*self.EMAIL)
    email_input.send_keys(email)
  
  def retrieve_password(self):
    retrieve_password_button = self.browser.find_element(*self.RETRIEVE_PASSWORD_BUTTON)
    retrieve_password_button.click()
  
  def get_confirmation_message(self):
    confirmation_message = self.browser.find_element(*self.CONFIRMATION_MESSAGE)
    return confirmation_message