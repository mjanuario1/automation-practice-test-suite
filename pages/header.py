from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class Header:

  # Locators

  SIGN_OUT_BUTTON = (By.CLASS_NAME, 'logout')
  FULL_NAME = (By.CLASS_NAME, 'account')

  # Initializer

  def __init__(self, browser):
    self.browser = browser

  # Interaction Methods

  def get_full_name(self):
    wait = WebDriverWait(self.browser, 10)
    full_name= wait.until(EC.visibility_of_element_located(self.FULL_NAME))
    return full_name

  def sign_out(self):
    sign_out_button = self.browser.find_element(*self.SIGN_OUT_BUTTON)
    print(sign_out_button)
    sign_out_button.click()